/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-netrounds',
      type: 'Netrounds',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Netrounds = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Netrounds Adapter Test', () => {
  describe('Netrounds Class Tests', () => {
    const a = new Netrounds(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-netrounds-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-netrounds-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const alarmEmailAccount = 'fakedata';
    const alarmEmailCreateAlarmEmailsBodyParam = {
      name: 'string'
    };
    describe('#createAlarmEmails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAlarmEmails(alarmEmailAccount, alarmEmailCreateAlarmEmailsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.addresses));
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlarmEmail', 'createAlarmEmails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAlarmEmails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAlarmEmails(alarmEmailAccount, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal(7, data.response.limit);
                assert.equal('string', data.response.next);
                assert.equal(3, data.response.offset);
                assert.equal('string', data.response.previous);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlarmEmail', 'listAlarmEmails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmEmailAlarmEmailsId = 555;
    const alarmEmailUpdateAlarmEmailsBodyParam = {
      name: 'string'
    };
    describe('#updateAlarmEmails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAlarmEmails(alarmEmailAccount, alarmEmailAlarmEmailsId, alarmEmailUpdateAlarmEmailsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlarmEmail', 'updateAlarmEmails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmEmails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlarmEmails(alarmEmailAccount, alarmEmailAlarmEmailsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.addresses));
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlarmEmail', 'getAlarmEmails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmTemplateAccount = 'fakedata';
    const alarmTemplateCreateAlarmTemplatesBodyParam = {
      name: 'string'
    };
    describe('#createAlarmTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAlarmTemplates(alarmTemplateAccount, alarmTemplateCreateAlarmTemplatesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.action);
                assert.equal(7, data.response.email);
                assert.equal(3600, data.response.interval);
                assert.equal(2, data.response.limit_per_monitoring);
                assert.equal(4, data.response.limit_per_test_interface);
                assert.equal('string', data.response.name);
                assert.equal(1, data.response.no_data_severity);
                assert.equal(3, data.response.no_data_timeout);
                assert.equal(8, data.response.snmp);
                assert.equal(8, data.response.thr_es_critical);
                assert.equal(9, data.response.thr_es_critical_clear);
                assert.equal(5, data.response.thr_es_major);
                assert.equal(4, data.response.thr_es_major_clear);
                assert.equal(3, data.response.thr_es_minor);
                assert.equal(6, data.response.thr_es_minor_clear);
                assert.equal(6, data.response.thr_es_warning);
                assert.equal(9, data.response.thr_es_warning_clear);
                assert.equal(3, data.response.window_size);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlarmTemplate', 'createAlarmTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAlarmTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAlarmTemplates(alarmTemplateAccount, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal(8, data.response.limit);
                assert.equal('string', data.response.next);
                assert.equal(9, data.response.offset);
                assert.equal('string', data.response.previous);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlarmTemplate', 'listAlarmTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmTemplateAlarmTemplateId = 555;
    const alarmTemplateUpdateAlarmTemplateBodyParam = {
      name: 'string'
    };
    describe('#updateAlarmTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAlarmTemplate(alarmTemplateAccount, alarmTemplateAlarmTemplateId, alarmTemplateUpdateAlarmTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlarmTemplate', 'updateAlarmTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlarmTemplate(alarmTemplateAccount, alarmTemplateAlarmTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.action);
                assert.equal(5, data.response.email);
                assert.equal(3600, data.response.interval);
                assert.equal(5, data.response.limit_per_monitoring);
                assert.equal(2, data.response.limit_per_test_interface);
                assert.equal('string', data.response.name);
                assert.equal(2, data.response.no_data_severity);
                assert.equal(2, data.response.no_data_timeout);
                assert.equal(1, data.response.snmp);
                assert.equal(6, data.response.thr_es_critical);
                assert.equal(6, data.response.thr_es_critical_clear);
                assert.equal(8, data.response.thr_es_major);
                assert.equal(5, data.response.thr_es_major_clear);
                assert.equal(4, data.response.thr_es_minor);
                assert.equal(1, data.response.thr_es_minor_clear);
                assert.equal(2, data.response.thr_es_warning);
                assert.equal(7, data.response.thr_es_warning_clear);
                assert.equal(9, data.response.window_size);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlarmTemplate', 'getAlarmTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmAccount = 'fakedata';
    describe('#listAlarms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAlarms(alarmAccount, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal(6, data.response.limit);
                assert.equal('string', data.response.next);
                assert.equal(1, data.response.offset);
                assert.equal('string', data.response.previous);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'listAlarms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmAlarmId = 555;
    const alarmUpdateAlarmBodyParam = {
      suppressed: true
    };
    describe('#updateAlarm - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAlarm(alarmAccount, alarmAlarmId, alarmUpdateAlarmBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'updateAlarm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarm - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlarm(alarmAccount, alarmAlarmId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.cleared);
                assert.equal('object', typeof data.response.config);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.interfaceName);
                assert.equal(1, data.response.max_es);
                assert.equal(4, data.response.max_severity);
                assert.equal('string', data.response.max_summary);
                assert.equal(4, data.response.severity);
                assert.equal('string', data.response.summary);
                assert.equal(false, data.response.suppressed);
                assert.equal(3, data.response.task);
                assert.equal(4, data.response.testAgent);
                assert.equal(1, data.response.total_es);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'getAlarm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iptvChannelAccount = 'fakedata';
    const iptvChannelCreateIptvChannelBodyParam = {
      ip: 'string',
      name: 'string',
      pnum: 7,
      port: 8
    };
    describe('#createIptvChannel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIptvChannel(iptvChannelAccount, iptvChannelCreateIptvChannelBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.ip);
                assert.equal('string', data.response.name);
                assert.equal(1, data.response.pnum);
                assert.equal(5, data.response.port);
                assert.equal('string', data.response.source);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IptvChannel', 'createIptvChannel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listIptvChannels - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listIptvChannels(iptvChannelAccount, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal(4, data.response.limit);
                assert.equal('string', data.response.next);
                assert.equal(8, data.response.offset);
                assert.equal('string', data.response.previous);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IptvChannel', 'listIptvChannels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iptvChannelIptvId = 555;
    const iptvChannelUpdateIptvChannelBodyParam = {
      ip: 'string',
      name: 'string',
      pnum: 5,
      port: 5
    };
    describe('#updateIptvChannel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIptvChannel(iptvChannelAccount, iptvChannelIptvId, iptvChannelUpdateIptvChannelBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IptvChannel', 'updateIptvChannel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIptvChannel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIptvChannel(iptvChannelAccount, iptvChannelIptvId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.ip);
                assert.equal('string', data.response.name);
                assert.equal(2, data.response.pnum);
                assert.equal(10, data.response.port);
                assert.equal('string', data.response.source);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IptvChannel', 'getIptvChannel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const monitorTemplateAccount = 'fakedata';
    describe('#listMonitorTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listMonitorTemplates(monitorTemplateAccount, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal(3, data.response.limit);
                assert.equal('string', data.response.next);
                assert.equal(9, data.response.offset);
                assert.equal('string', data.response.previous);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MonitorTemplate', 'listMonitorTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const monitorTemplateTemplateId = 555;
    describe('#getMonitorTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMonitorTemplate(monitorTemplateAccount, monitorTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.description);
                assert.equal(9, data.response.id);
                assert.equal('object', typeof data.response.inputs);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MonitorTemplate', 'getMonitorTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const monitorAccount = 'fakedata';
    const monitorCreateMonitorBodyParam = {
      alarm_configs: [
        {}
      ],
      description: 'string',
      input_values: {},
      name: 'string',
      started: true,
      template_id: 8
    };
    describe('#createMonitor - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createMonitor(monitorAccount, monitorCreateMonitorBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.alarm_configs));
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.gui_url);
                assert.equal(4, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(true, data.response.started);
                assert.equal(6, data.response.template_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Monitor', 'createMonitor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listMonitors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listMonitors(monitorAccount, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal(6, data.response.limit);
                assert.equal('string', data.response.next);
                assert.equal(6, data.response.offset);
                assert.equal('string', data.response.previous);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Monitor', 'listMonitors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const monitorMonitorId = 555;
    const monitorUpdateMonitorBodyParam = {
      name: 'string'
    };
    describe('#updateMonitor - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateMonitor(monitorAccount, monitorMonitorId, monitorUpdateMonitorBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Monitor', 'updateMonitor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const monitorPatchMonitorBodyParam = {
      description: 'string',
      name: 'string',
      started: true
    };
    describe('#patchMonitor - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchMonitor(monitorAccount, monitorMonitorId, monitorPatchMonitorBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Monitor', 'patchMonitor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitor - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMonitor(monitorAccount, monitorMonitorId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.alarm_configs));
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.gui_url);
                assert.equal(10, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.tasks));
                assert.equal(8, data.response.template_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Monitor', 'getMonitor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitorPdfreport - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMonitorPdfreport(monitorAccount, monitorMonitorId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netrounds-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Monitor', 'getMonitorPdfreport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sipAccountAccount = 'fakedata';
    const sipAccountCreateSipAccountBodyParam = {
      password: 'string',
      sip_domain: 'string',
      username: 'string'
    };
    describe('#createSipAccount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSipAccount(sipAccountAccount, sipAccountCreateSipAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.password);
                assert.equal('string', data.response.proxy);
                assert.equal('string', data.response.registrar);
                assert.equal('string', data.response.sip_domain);
                assert.equal('string', data.response.uri_rewrite);
                assert.equal('string', data.response.user_auth);
                assert.equal('string', data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SipAccount', 'createSipAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSipAccounts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listSipAccounts(sipAccountAccount, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal(8, data.response.limit);
                assert.equal('string', data.response.next);
                assert.equal(4, data.response.offset);
                assert.equal('string', data.response.previous);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SipAccount', 'listSipAccounts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sipAccountSipId = 555;
    const sipAccountUpdateSipAccountBodyParam = {
      password: 'string',
      sip_domain: 'string',
      username: 'string'
    };
    describe('#updateSipAccount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateSipAccount(sipAccountAccount, sipAccountSipId, sipAccountUpdateSipAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SipAccount', 'updateSipAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSipAccount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSipAccount(sipAccountAccount, sipAccountSipId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.password);
                assert.equal('string', data.response.proxy);
                assert.equal('string', data.response.registrar);
                assert.equal('string', data.response.sip_domain);
                assert.equal('string', data.response.uri_rewrite);
                assert.equal('string', data.response.user_auth);
                assert.equal('string', data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SipAccount', 'getSipAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snmpManagerAccount = 'fakedata';
    const snmpManagerCreateSnmpManagerBodyParam = {
      ip: 'string',
      name: 'string',
      version: '3'
    };
    describe('#createSnmpManager - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSnmpManager(snmpManagerAccount, snmpManagerCreateSnmpManagerBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.auth_password);
                assert.equal('string', data.response.community);
                assert.equal('string', data.response.engine_id);
                assert.equal('string', data.response.ip);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.priv_password);
                assert.equal('auth_priv', data.response.security);
                assert.equal('string', data.response.user_name);
                assert.equal('3', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnmpManager', 'createSnmpManager', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSnmpManagers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listSnmpManagers(snmpManagerAccount, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal(3, data.response.limit);
                assert.equal('string', data.response.next);
                assert.equal(10, data.response.offset);
                assert.equal('string', data.response.previous);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnmpManager', 'listSnmpManagers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snmpManagerSnmpManagerId = 555;
    const snmpManagerUpdateSnmpManagerBodyParam = {
      ip: 'string',
      name: 'string',
      version: '3'
    };
    describe('#updateSnmpManager - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateSnmpManager(snmpManagerAccount, snmpManagerSnmpManagerId, snmpManagerUpdateSnmpManagerBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnmpManager', 'updateSnmpManager', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnmpManager - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSnmpManager(snmpManagerAccount, snmpManagerSnmpManagerId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.auth_password);
                assert.equal('string', data.response.community);
                assert.equal('string', data.response.engine_id);
                assert.equal('string', data.response.ip);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.priv_password);
                assert.equal('no_auth_no_priv', data.response.security);
                assert.equal('string', data.response.user_name);
                assert.equal('2c', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnmpManager', 'getSnmpManager', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const testAgentAccount = 'fakedata';
    const testAgentCreateTestAgentBodyParam = {
      name: 'string',
      type: 'appliance'
    };
    describe('#createTestAgent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTestAgent(testAgentAccount, testAgentCreateTestAgentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.cpu);
                assert.equal('string', data.response.description);
                assert.equal(6, data.response.gps_lat);
                assert.equal(10, data.response.gps_long);
                assert.equal('object', typeof data.response.interface_config);
                assert.equal('object', typeof data.response.interface_states);
                assert.equal(true, Array.isArray(data.response.load_avg));
                assert.equal('string', data.response.management_interface);
                assert.equal(2, data.response.memory);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.ntp_config);
                assert.equal(true, data.response.online);
                assert.equal('appliance', data.response.type);
                assert.equal('string', data.response.update_error);
                assert.equal(true, data.response.updating);
                assert.equal(2, data.response.uptime);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TestAgent', 'createTestAgent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const testAgentRebootTestAgentBodyParam = {
      test_agent_ids: [
        7
      ]
    };
    describe('#rebootTestAgent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rebootTestAgent(testAgentAccount, null, testAgentRebootTestAgentBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netrounds-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TestAgent', 'rebootTestAgent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const testAgentUpgradeTestAgentSoftwareBodyParam = {
      test_agent_ids: [
        8
      ]
    };
    describe('#upgradeTestAgentSoftware - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.upgradeTestAgentSoftware(testAgentAccount, null, testAgentUpgradeTestAgentSoftwareBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netrounds-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TestAgent', 'upgradeTestAgentSoftware', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTestAgents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTestAgents(testAgentAccount, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal(2, data.response.limit);
                assert.equal('string', data.response.next);
                assert.equal(9, data.response.offset);
                assert.equal('string', data.response.previous);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TestAgent', 'listTestAgents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const testAgentTestAgentId = 'fakedata';
    const testAgentUpdateTestAgentBodyParam = {
      name: 'string',
      type: 'lite'
    };
    describe('#updateTestAgent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTestAgent(testAgentAccount, testAgentTestAgentId, testAgentUpdateTestAgentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TestAgent', 'updateTestAgent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const testAgentPatchTestAgentBodyParam = {
      description: 'string',
      gps_lat: 8,
      gps_long: 10,
      name: 'string'
    };
    describe('#patchTestAgent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchTestAgent(testAgentAccount, testAgentTestAgentId, testAgentPatchTestAgentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TestAgent', 'patchTestAgent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTestAgent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTestAgent(testAgentAccount, testAgentTestAgentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.cpu);
                assert.equal('string', data.response.description);
                assert.equal(5, data.response.gps_lat);
                assert.equal(4, data.response.gps_long);
                assert.equal('object', typeof data.response.interface_config);
                assert.equal('object', typeof data.response.interface_states);
                assert.equal(true, Array.isArray(data.response.load_avg));
                assert.equal('string', data.response.management_interface);
                assert.equal(8, data.response.memory);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.ntp_config);
                assert.equal(false, data.response.online);
                assert.equal('appliance', data.response.type);
                assert.equal('string', data.response.update_error);
                assert.equal(false, data.response.updating);
                assert.equal(6, data.response.uptime);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TestAgent', 'getTestAgent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const testAgentTestAgentNumericId = 555;
    const testAgentTestAgentInterfaceName = 'fakedata';
    describe('#scanWifiNetworksStart - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.scanWifiNetworksStart(testAgentAccount, testAgentTestAgentNumericId, testAgentTestAgentInterfaceName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netrounds-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TestAgent', 'scanWifiNetworksStart', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scanWifiNetworksResults - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.scanWifiNetworksResults(testAgentAccount, testAgentTestAgentNumericId, testAgentTestAgentInterfaceName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netrounds-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TestAgent', 'scanWifiNetworksResults', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const testTemplateAccount = 'fakedata';
    describe('#listTestTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTestTemplates(testTemplateAccount, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal(2, data.response.limit);
                assert.equal('string', data.response.next);
                assert.equal(5, data.response.offset);
                assert.equal('string', data.response.previous);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TestTemplate', 'listTestTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const testTemplateTemplateId = 555;
    describe('#getTestTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTestTemplate(testTemplateAccount, testTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.description);
                assert.equal(1, data.response.id);
                assert.equal('object', typeof data.response.inputs);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TestTemplate', 'getTestTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const testAccount = 'fakedata';
    const testCreateTestBodyParam = {
      description: 'string',
      input_values: {},
      name: 'string',
      status: 'scheduled',
      template_id: 5
    };
    describe('#createTest - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTest(testAccount, testCreateTestBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.gui_url);
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(9, data.response.template_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Test', 'createTest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTests - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTests(testAccount, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal(3, data.response.limit);
                assert.equal('string', data.response.next);
                assert.equal(9, data.response.offset);
                assert.equal('string', data.response.previous);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Test', 'listTests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const testTestId = 555;
    describe('#getTest - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTest(testAccount, testTestId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.end_time);
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.report_url);
                assert.equal('string', data.response.report_url_pdf);
                assert.equal('string', data.response.start_time);
                assert.equal('scheduled', data.response.status);
                assert.equal(true, Array.isArray(data.response.steps));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Test', 'getTest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTestPdfreport - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTestPdfreport(testAccount, testTestId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netrounds-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Test', 'getTestPdfreport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const twampReflectorAccount = 'fakedata';
    const twampReflectorCreateTwampReflectorBodyParam = {
      host: 'string',
      name: 'string',
      port: 8
    };
    describe('#createTwampReflector - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTwampReflector(twampReflectorAccount, twampReflectorCreateTwampReflectorBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.ctrl_port);
                assert.equal(5, data.response.gps_lat);
                assert.equal(10, data.response.gps_long);
                assert.equal('string', data.response.host);
                assert.equal(false, data.response.ipv6);
                assert.equal('string', data.response.name);
                assert.equal(5, data.response.port);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TwampReflector', 'createTwampReflector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTwampReflectors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTwampReflectors(twampReflectorAccount, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal(3, data.response.limit);
                assert.equal('string', data.response.next);
                assert.equal(9, data.response.offset);
                assert.equal('string', data.response.previous);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TwampReflector', 'listTwampReflectors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const twampReflectorTwampId = 555;
    const twampReflectorUpdateTwampReflectorBodyParam = {
      host: 'string',
      name: 'string',
      port: 6
    };
    describe('#updateTwampReflector - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTwampReflector(twampReflectorAccount, twampReflectorTwampId, twampReflectorUpdateTwampReflectorBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TwampReflector', 'updateTwampReflector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTwampReflector - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTwampReflector(twampReflectorAccount, twampReflectorTwampId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.ctrl_port);
                assert.equal(9, data.response.gps_lat);
                assert.equal(10, data.response.gps_long);
                assert.equal('string', data.response.host);
                assert.equal(false, data.response.ipv6);
                assert.equal('string', data.response.name);
                assert.equal(6, data.response.port);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TwampReflector', 'getTwampReflector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const y1731MepAccount = 'fakedata';
    const y1731MepCreateY1731MepBodyParam = {
      mac: 'string',
      meg_level: 3,
      name: 'string'
    };
    describe('#createY1731Mep - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createY1731Mep(y1731MepAccount, y1731MepCreateY1731MepBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.mac);
                assert.equal(4, data.response.meg_level);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Y1731Mep', 'createY1731Mep', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listY1731Meps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listY1731Meps(y1731MepAccount, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal(6, data.response.limit);
                assert.equal('string', data.response.next);
                assert.equal(7, data.response.offset);
                assert.equal('string', data.response.previous);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Y1731Mep', 'listY1731Meps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const y1731MepMepId = 555;
    const y1731MepUpdateY1731MepBodyParam = {
      mac: 'string',
      meg_level: 10,
      name: 'string'
    };
    describe('#updateY1731Mep - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateY1731Mep(y1731MepAccount, y1731MepMepId, y1731MepUpdateY1731MepBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netrounds-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Y1731Mep', 'updateY1731Mep', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getY1731Mep - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getY1731Mep(y1731MepAccount, y1731MepMepId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.mac);
                assert.equal(9, data.response.meg_level);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Y1731Mep', 'getY1731Mep', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAlarmEmails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAlarmEmails(alarmEmailAccount, alarmEmailAlarmEmailsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netrounds-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlarmEmail', 'deleteAlarmEmails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAlarmTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAlarmTemplate(alarmTemplateAccount, alarmTemplateAlarmTemplateId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netrounds-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlarmTemplate', 'deleteAlarmTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAlarm - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAlarm(alarmAccount, alarmAlarmId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netrounds-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'deleteAlarm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIptvChannel - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIptvChannel(iptvChannelAccount, iptvChannelIptvId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netrounds-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IptvChannel', 'deleteIptvChannel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMonitor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteMonitor(monitorAccount, monitorMonitorId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netrounds-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Monitor', 'deleteMonitor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSipAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSipAccount(sipAccountAccount, sipAccountSipId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netrounds-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SipAccount', 'deleteSipAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSnmpManager - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSnmpManager(snmpManagerAccount, snmpManagerSnmpManagerId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netrounds-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnmpManager', 'deleteSnmpManager', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTestAgent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTestAgent(testAgentAccount, testAgentTestAgentId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netrounds-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TestAgent', 'deleteTestAgent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scanWifiNetworksStop - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.scanWifiNetworksStop(testAgentAccount, testAgentTestAgentNumericId, testAgentTestAgentInterfaceName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netrounds-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TestAgent', 'scanWifiNetworksStop', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTest(testAccount, testTestId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netrounds-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Test', 'deleteTest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTwampReflector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTwampReflector(twampReflectorAccount, twampReflectorTwampId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netrounds-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TwampReflector', 'deleteTwampReflector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteY1731Mep - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteY1731Mep(y1731MepAccount, y1731MepMepId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netrounds-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Y1731Mep', 'deleteY1731Mep', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
