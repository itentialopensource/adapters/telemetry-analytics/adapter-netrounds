
## 0.4.2 [08-07-2024]

* finish deprecation

See merge request itentialopensource/adapters/adapter-netrounds!14

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_21:09PM

See merge request itentialopensource/adapters/adapter-netrounds!13

---

## 0.4.0 [07-03-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-netrounds!12

---

## 0.3.6 [03-27-2024]

* Changes made at 2024.03.27_13:21PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-netrounds!11

---

## 0.3.5 [03-21-2024]

* Changes made at 2024.03.21_14:56PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-netrounds!10

---

## 0.3.4 [03-13-2024]

* Changes made at 2024.03.13_12:48PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-netrounds!9

---

## 0.3.3 [03-12-2024]

* Changes made at 2024.03.12_11:03AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-netrounds!8

---

## 0.3.2 [02-27-2024]

* Changes made at 2024.02.27_11:38AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-netrounds!7

---

## 0.3.1 [01-19-2024]

* deprecation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-netrounds!6

---

## 0.3.0 [01-03-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-netrounds!5

---

## 0.2.0 [05-28-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-netrounds!4

---

## 0.1.5 [03-11-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/telemetry-analytics/adapter-netrounds!3

---

## 0.1.4 [07-08-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-netrounds!2

---

## 0.1.3 [01-13-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-netrounds!1

---

## 0.1.2 [12-09-2019] & 0.1.1 [12-09-2019]

- Initial Commit

See commit f31cf21

---
