## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Netrounds. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Netrounds.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Netrounds. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">listAlarmEmails(account, limit, offset, callback)</td>
    <td style="padding:15px">List alarm emails.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarm_emails/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAlarmEmails(account, body, callback)</td>
    <td style="padding:15px">Create alarm emails.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarm_emails/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAlarmEmails(account, alarmEmailsId, callback)</td>
    <td style="padding:15px">Delete alarm emails instance.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarm_emails/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmEmails(account, alarmEmailsId, callback)</td>
    <td style="padding:15px">Return alarm emails instance.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarm_emails/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlarmEmails(account, alarmEmailsId, body, callback)</td>
    <td style="padding:15px">Modify existing alarm emails.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarm_emails/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAlarmTemplates(account, limit, offset, callback)</td>
    <td style="padding:15px">List Alarm Templates.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarm_templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAlarmTemplates(account, body, callback)</td>
    <td style="padding:15px">Create Alarm Templates.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarm_templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAlarmTemplate(account, alarmTemplateId, callback)</td>
    <td style="padding:15px">Delete an Alarm Template instance.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarm_templates/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmTemplate(account, alarmTemplateId, callback)</td>
    <td style="padding:15px">Return an Alarm Template instance.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarm_templates/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlarmTemplate(account, alarmTemplateId, body, callback)</td>
    <td style="padding:15px">Modify an existing Alarm Template.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarm_templates/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAlarms(account, limit, offset, createdTimespanStart, createdTimespanEnd, clearedTimespanStart, clearedTimespanEnd, suppressed, interfaceName, testAgent, task, callback)</td>
    <td style="padding:15px">List alarms.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarms/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAlarm(account, alarmId, callback)</td>
    <td style="padding:15px">Delete alarm instance.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarms/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarm(account, alarmId, callback)</td>
    <td style="padding:15px">Return alarm instance.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarms/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlarm(account, alarmId, body, callback)</td>
    <td style="padding:15px">Suppress existing alarm.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarms/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listIptvChannels(account, limit, offset, callback)</td>
    <td style="padding:15px">List IPTV Channels</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/iptv_channels/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIptvChannel(account, body, callback)</td>
    <td style="padding:15px">Creates an IPTV Channel</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/iptv_channels/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIptvChannel(account, iptvId, callback)</td>
    <td style="padding:15px">Deletes an IPTV Channel instance</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/iptv_channels/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIptvChannel(account, iptvId, callback)</td>
    <td style="padding:15px">Returns an IPTV Channel instance</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/iptv_channels/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIptvChannel(account, iptvId, body, callback)</td>
    <td style="padding:15px">Modifies an IPTV Channel</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/iptv_channels/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listMonitorTemplates(account, limit, offset, callback)</td>
    <td style="padding:15px">List Monitor Templates</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/monitor_templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitorTemplate(account, templateId, callback)</td>
    <td style="padding:15px">Returns a Monitor Template Instance</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/monitor_templates/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listMonitors(account, limit, offset, start, end, callback)</td>
    <td style="padding:15px">List Monitors</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/monitors/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMonitor(account, body, callback)</td>
    <td style="padding:15px">Creates new Monitor from template</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/monitors/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMonitor(account, monitorId, callback)</td>
    <td style="padding:15px">Deletes a Monitor instance</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/monitors/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitor(account, monitorId, start, end, resolution, withDetailedMetrics, withMetricsAvg, withOtherResults, callback)</td>
    <td style="padding:15px">Get a Monitor and its SLA.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/monitors/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchMonitor(account, monitorId, body, callback)</td>
    <td style="padding:15px">Modify Monitor instance using a PATCH method</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/monitors/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMonitor(account, monitorId, body, callback)</td>
    <td style="padding:15px">Modify Monitor instance using a PUT method</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/monitors/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitorPdfreport(account, monitorId, start, end, worstNum, group, graphs, callback)</td>
    <td style="padding:15px">Get a Monitor PDF report.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/monitors/{pathv2}/pdf_report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSipAccounts(account, limit, offset, callback)</td>
    <td style="padding:15px">List SIP Accounts</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/sip_accounts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSipAccount(account, body, callback)</td>
    <td style="padding:15px">Creates a new SIP Account</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/sip_accounts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSipAccount(account, sipId, callback)</td>
    <td style="padding:15px">Deletes a SIP Account instance</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/sip_accounts/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSipAccount(account, sipId, callback)</td>
    <td style="padding:15px">Returns a SIP Account instance</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/sip_accounts/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSipAccount(account, sipId, body, callback)</td>
    <td style="padding:15px">Modifies a SIP Account</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/sip_accounts/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSnmpManagers(account, limit, offset, callback)</td>
    <td style="padding:15px">List SNMP managers.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/snmp_managers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSnmpManager(account, body, callback)</td>
    <td style="padding:15px">Create SNMP manager.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/snmp_managers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSnmpManager(account, snmpManagerId, callback)</td>
    <td style="padding:15px">Delete SNMP manager instance.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/snmp_managers/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSnmpManager(account, snmpManagerId, callback)</td>
    <td style="padding:15px">Return SNMP manager instance.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/snmp_managers/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSnmpManager(account, snmpManagerId, body, callback)</td>
    <td style="padding:15px">Modify existing SNMP manager.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/snmp_managers/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTestAgents(account, limit, offset, callback)</td>
    <td style="padding:15px">Lists Test Agents</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTestAgent(account, body, callback)</td>
    <td style="padding:15px">Creates a new Test Agent</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rebootTestAgent(account, all, body, callback)</td>
    <td style="padding:15px">Reboot Test Agents</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/reboot/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upgradeTestAgentSoftware(account, all, body, callback)</td>
    <td style="padding:15px">Update Test Agents</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/update/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTestAgent(account, testAgentId, callback)</td>
    <td style="padding:15px">Deletes a Test Agent Instance</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTestAgent(account, testAgentId, callback)</td>
    <td style="padding:15px">Returns a Test Agent Instance</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTestAgent(account, testAgentId, body, callback)</td>
    <td style="padding:15px">Modifies a Test Agent Instance using PATCH method.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTestAgent(account, testAgentId, body, callback)</td>
    <td style="padding:15px">Modifies a Test Agent Instance</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scanWifiNetworksStop(account, testAgentNumericId, testAgentInterfaceName, callback)</td>
    <td style="padding:15px">Terminates the Wi-Fi scanning process</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/{pathv2}/wifiscan/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scanWifiNetworksResults(account, testAgentNumericId, testAgentInterfaceName, callback)</td>
    <td style="padding:15px">Gets Wi-Fi scanning results</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/{pathv2}/wifiscan/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scanWifiNetworksStart(account, testAgentNumericId, testAgentInterfaceName, callback)</td>
    <td style="padding:15px">Starts the Wi-Fi scanning process</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/{pathv2}/wifiscan/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTestTemplates(account, limit, offset, callback)</td>
    <td style="padding:15px">List Test Templates</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTestTemplate(account, templateId, callback)</td>
    <td style="padding:15px">Returns a Test Template Instance</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_templates/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTests(account, limit, offset, callback)</td>
    <td style="padding:15px">List Tests</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/tests/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTest(account, body, callback)</td>
    <td style="padding:15px">Creates new Test from template</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/tests/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTest(account, testId, callback)</td>
    <td style="padding:15px">Deletes a Test instance</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/tests/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTest(account, testId, withDetailedMetrics, withMetricsAvg, withOtherResults, callback)</td>
    <td style="padding:15px">Returns a Test Instance</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/tests/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTestPdfreport(account, testId, start, end, worstNum, group, graphs, callback)</td>
    <td style="padding:15px">Get a Test PDF report.</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/tests/{pathv2}/pdf_report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTwampReflectors(account, limit, offset, callback)</td>
    <td style="padding:15px">List TWAMP Reflectors</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/twamp_reflectors/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTwampReflector(account, body, callback)</td>
    <td style="padding:15px">Creates a new TWAMP Reflector</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/twamp_reflectors/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTwampReflector(account, twampId, callback)</td>
    <td style="padding:15px">Deletes a TWAMP Reflector Instance</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/twamp_reflectors/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTwampReflector(account, twampId, callback)</td>
    <td style="padding:15px">Returns a TWAMP Reflector Instance</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/twamp_reflectors/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTwampReflector(account, twampId, body, callback)</td>
    <td style="padding:15px">Modifies a TWAMP Reflector</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/twamp_reflectors/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listY1731Meps(account, limit, offset, callback)</td>
    <td style="padding:15px">List Y1731 MEPs</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/y1731_meps/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createY1731Mep(account, body, callback)</td>
    <td style="padding:15px">Creates a new Y1731 MEP</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/y1731_meps/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteY1731Mep(account, mepId, callback)</td>
    <td style="padding:15px">Deletes a Y1731 MEP Instance</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/y1731_meps/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getY1731Mep(account, mepId, callback)</td>
    <td style="padding:15px">Returns a Y1731 MEP Instance</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/y1731_meps/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateY1731Mep(account, mepId, body, callback)</td>
    <td style="padding:15px">Modifies a Y1731 MEP</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/y1731_meps/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
