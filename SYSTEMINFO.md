# Netrounds

Vendor: Juniper Networks
Homepage: https://www.juniper.net/us/en.html

Product: Netrounds
Product Page: https://www.juniper.net/us/en/products/network-automation/paragon-active-assurance.html

## Introduction
We classify Netrounds into the Service Assurance domain as Netrounds provides information on Events and Monitoring of Networks. 

Netrounds has been replaced with Juniper Paragon Active Assurance and so this adapter has been deprecated and will eventually be archived.

## Why Integrate
The Netrounds adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Netrounds to offer a Monitoring, Events and Analytics. 

With this adapter you have the ability to perform operations with Netrounds such as:

- Alarm
- Events
- Testing

## Additional Product Documentation
The [Netrounds REST API](https://app.netrounds.com/rest/)

